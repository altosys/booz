# booz

A web scraper for parsing and notifying of systembolaget.se malt whisky releases.

## Usage

booz is configured using environment variables, see the table below for available parameters

For an example configuration refer to the `docker-compose.yml` file in the [repo](https://gitlab.com/altosys/booz).

| Environment variable      | Description                                        | Type   | Default                                                                                      |
|---------------------------|----------------------------------------------------|--------|----------------------------------------------------------------------------------------------|
| BOOZ_SMALL_BATCH_RELEASES | Enable monitoring of small batch releases          | String | false                                                                                        |
| BOOZ_WEB_RELEASES         | Enable monitoring of web releases                  | String | false                                                                                        |
| BOOZ_PUSHOVER_API_TOKEN   | Pushover API application token                     | String | -                                                                                            |
| BOOZ_PUSHOVER_USER_KEY    | Pushover API user key                              | String | -                                                                                            |
| BOOZ_SCHEDULE             | Schedule for fetching release data, in cron format | String | "0 9 \* \* 3"                                                                                |
| BOOZ_TZ                   | Linux time zone                                    | String | UTC ([See list of time zones](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)) |

## Screenshots

<img src="https://gitlab.com/altosys/booz/-/wikis/images/booz.jpeg" alt="booz notification" width="400"/>

## License

booz is licensed under the terms of [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
