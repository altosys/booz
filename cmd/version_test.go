package cmd

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPrintVersion(t *testing.T) {
	printVersion(nil, []string{})
}

func TestGetVersionStr(t *testing.T) {
	vs := getVersionStr()
	assert.Contains(t, vs, "builder")
	assert.Contains(t, vs, "commit")
	assert.Contains(t, vs, "date")
	assert.Contains(t, vs, "version")
}
