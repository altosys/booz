package cmd

import (
	"booz"
	"booz/config"
	"booz/notify"
	"booz/scraper"
	"booz/utils"
	"context"
	"log"
	"os"
	"sync"

	"github.com/go-co-op/gocron"
	"github.com/spf13/cobra"
)

var splash = `
 _
| |_ ___ ___ ___
| . | . | . |- _|
|___|___|___|___|
══════════════════════════════════════════════════════════════════════════════════════════
`

var (
	ctx          context.Context
	cancel       context.CancelFunc
	ready        sync.Mutex
	scheduler    *gocron.Scheduler
	now          bool
	skipNotify   bool
	printResults bool
)

var runCmd = &cobra.Command{
	Use:     "run",
	Short:   "Run booz",
	Long:    "Run booz service",
	Version: booz.Version,
	Run:     execRun,
}

func init() {
	runCmd.Flags().BoolVarP(&now, "now", "n", false, "Run now (skip schedule)")
	runCmd.Flags().BoolVarP(&skipNotify, "skip-notify", "s", false, "Skip sending notifications")
	runCmd.Flags().BoolVarP(&printResults, "print-results", "p", false, "Print results to console")
}

func execRun(cmd *cobra.Command, args []string) {
	ready.Lock()
	ctx, cancel = context.WithCancel(context.Background())
	run(ctx, cancel)
}

func run(ctx context.Context, cancel context.CancelFunc) {
	os.Stderr.WriteString(splash)
	log.Println("version:", booz.Version)
	go utils.StartInterruptListener(ctx, cancel)
	cfg, err := config.Load(&config.LoadOptions{SkipPushoverCredentials: skipNotify})
	if err != nil {
		log.Fatalln("invalid config:", err)
	}

	releases := []*booz.Release{}
	if cfg.GetWebReleasesEnabled() {
		releases = append(releases, &booz.Release{Type: booz.WebRelease})
	}
	if cfg.GetSmallBatchReleasesEnabled() {
		releases = append(releases, &booz.Release{Type: booz.SmallBatchRelease})
	}

	cronFn := func() {
		log.Println("scrape triggered, starting run")
		notifier := notify.NewNotifier(cfg.GetPushoverUserKey(), cfg.GetPushoverAPIToken())
		scraper.UpdateReleases(releases)
		s := scraper.GetScraper()

		for _, r := range releases {
			products, err := s.ScrapeRelease(r)
			if err != nil {
				log.Println("scraper:", err)
			}

			if printResults {
				log.Println("results:")
				for _, p := range products {
					log.Println(p)
				}
			}

			if !skipNotify {
				if err := notifier.NotifyReleaseProducts(products, r); err != nil {
					log.Println("notification:", err)
				}
			}
		}
		log.Println("finished run")
	}

	if now {
		cronFn()
		return
	}

	scheduler = gocron.NewScheduler(cfg.GetTimeZone())
	if _, err := scheduler.Cron(cfg.GetSchedule()).Do(func() { cronFn() }); err != nil {
		log.Fatalln("cron:", err)
	}

	scheduler.StartAsync()
	log.Println("running on schedule:", cfg.GetSchedule())
	log.Println("monitoring web releases:", cfg.GetWebReleasesEnabled())
	log.Println("monitoring small batch releases:", cfg.GetSmallBatchReleasesEnabled())
	ready.Unlock()

	<-ctx.Done()
	log.Println("stopping scheduler...")
	scheduler.Stop()
	log.Println("successfully stopped scheduler")
	log.Println("shutdown complete")
}
