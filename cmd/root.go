package cmd

import (
	"booz"

	cobra "github.com/spf13/cobra"
)

// RootCmd displays basic information about the CLI usage
var rootCmd = &cobra.Command{
	Use:     "",
	Short:   "booz",
	Long:    "booz is a systembolaget.se web release scraper",
	Version: booz.Version,
	Run:     usage,
}

// Execute runs the root command of the command tree
func Execute() {
	rootCmd.PersistentFlags().BoolP("help", "h", false, "Help for the command")
	rootCmd.PersistentFlags().BoolP("version", "v", false, "Show version information")

	rootCmd.SetVersionTemplate(getVersionStr())
	rootCmd.AddCommand(versionCmd)
	rootCmd.AddCommand(runCmd)

	//nolint:errcheck
	rootCmd.Execute()
}

func usage(cmd *cobra.Command, args []string) {
	//nolint:errcheck
	cmd.Usage()
}
