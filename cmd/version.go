package cmd

import (
	"booz"
	"bytes"
	"fmt"

	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Display version information",
	Long:  "Display version information and other build metadata",
	Run:   printVersion,
}

func printVersion(cmd *cobra.Command, args []string) {
	fmt.Println(getVersionStr())
}

func getVersionStr() string {
	data := [][]string{
		{"builder:", booz.Builder},
		{"commit:", booz.Commit},
		{"date:", booz.Date},
		{"version:", booz.Version},
	}

	buf := new(bytes.Buffer)
	table := tablewriter.NewWriter(buf)
	table.SetAutoWrapText(false)
	table.SetBorder(false)
	table.SetTablePadding(" ")
	table.SetNoWhiteSpace(true)
	table.AppendBulk(data)
	table.Render()
	return buf.String()
}
