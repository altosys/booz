package cmd

import (
	"os"
	"testing"
	"time"

	cobra "github.com/spf13/cobra"
	"github.com/stretchr/testify/assert"
)

func TestCmd(t *testing.T) {
	assert.NoError(t, rootCmd.Execute())
	assert.NoError(t, versionCmd.Execute())
}

func TestExecute(t *testing.T) {
	Execute()
}

func TestExecRun(t *testing.T) {
	setupTestEnv()
	defer clearTestEnv()

	// execRun unlocks mutex when ready
	go execRun(&cobra.Command{}, []string{})
	time.Sleep(100 * time.Millisecond) // allow execRun time to grab lock
	ready.Lock()
	defer ready.Unlock()

	scheduler.RunAll()
	job := scheduler.Jobs()[0]
	assert.NotNil(t, job)
	cancel()
}

func setupTestEnv() {
	clearTestEnv()
	os.Setenv("BOOZ_SCHEDULE", "1 1 1 1 1")
	os.Setenv("BOOZ_PUSHOVER_API_TOKEN", "apitoken")
	os.Setenv("BOOZ_PUSHOVER_USER_KEY", "userkey")
	os.Setenv("BOOZ_WEB_RELEASES", "true")
}

func clearTestEnv() {
	os.Unsetenv("BOOZ_SCHEDULE")
	os.Unsetenv("BOOZ_PUSHOVER_API_TOKEN")
	os.Unsetenv("BOOZ_PUSHOVER_USER_KEY")
	os.Unsetenv("BOOZ_WEB_RELEASES")
}
