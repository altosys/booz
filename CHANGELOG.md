
<a name="v1.2.3"></a>
## [v1.2.3](https://gitlab.com/altosys/booz/compare/v1.2.2...v1.2.3) (2023-04-02)

### Chore

* **deps:** update chromedp version
* **gitignore:** add .vscode

### Ci

* update podman configuration

### Fix

* **scraper:** update age confirmation and cookie dialogs handling
* **scraper:** include irish spelling for whiskey
* **scraper:** add support to pass age confirmation prompt


<a name="v1.2.2"></a>
## [v1.2.2](https://gitlab.com/altosys/booz/compare/v1.2.1...v1.2.2) (2023-02-23)

### Chore

* improve '--print-results' output format
* **release:** v1.2.2 [skip ci]

### Fix

* **scraper:** update ABV parsing to match upstream format


<a name="v1.2.1"></a>
## [v1.2.1](https://gitlab.com/altosys/booz/compare/v1.2.0...v1.2.1) (2023-02-15)

### Chore

* **release:** v1.2.1 [skip ci]

### Fix

* **config:** ignore unset credentials when skipping notifications


<a name="v1.2.0"></a>
## [v1.2.0](https://gitlab.com/altosys/booz/compare/v1.1.2...v1.2.0) (2023-02-09)

### Chore

* **release:** v1.2.0 [skip ci]

### Feat

* allow running without schedule and notifications


<a name="v1.1.2"></a>
## [v1.1.2](https://gitlab.com/altosys/booz/compare/v1.1.1...v1.1.2) (2022-12-28)

### Chore

* **release:** v1.1.2 [skip ci]

### Fix

* prevent panic on empty scrape result


<a name="v1.1.1"></a>
## [v1.1.1](https://gitlab.com/altosys/booz/compare/v1.1.0...v1.1.1) (2022-11-20)

### Chore

* **release:** v1.1.1 [skip ci]

### Fix

* print monitoring configuration on startup


<a name="v1.1.0"></a>
## [v1.1.0](https://gitlab.com/altosys/booz/compare/v1.0.1...v1.1.0) (2022-11-20)

### Chore

* update toolkit reference
* **release:** v1.1.0 [skip ci]

### Ci

* override pipeline test jobs to support chromedp
* update template configurations

### Feat

* add monitoring of small batch releases


<a name="v1.0.1"></a>
## [v1.0.1](https://gitlab.com/altosys/booz/compare/v1.0.0...v1.0.1) (2022-10-15)

### Chore

* **release:** v1.0.1 [skip ci]

### Fix

* **scraper:** update css selector to match upstream changes


<a name="v1.0.0"></a>
## v1.0.0 (2022-09-02)

### Chore

* add .gitignore
* add toolkit submodule and Taskfile.yml
* initial commit
* **release:** v1.0.0 [skip ci]

### Ci

* set up pipeline

### Docs

* add link to gitlab repo to README.md
* add LICENSE
* update README.md

