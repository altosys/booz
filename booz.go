package booz

import "time"

type Product struct {
	Brand, Country, Bottling string
	ID, Price, Volume        int
	ABV                      float32
}

type Release struct {
	Date time.Time
	URL  string
	Type ReleaseType
}

type ReleaseType string

const (
	WebRelease        ReleaseType = "Web"
	SmallBatchRelease ReleaseType = "Small Batch"
)

type Scraper interface {
	ScrapeRelease(release *Release) ([]Product, error)
}

var (
	// Builder is the identity of the builder of the binary
	Builder = "n/a"
	// Commit is the git commit sha of the current commit
	Commit = "n/a"
	// Date is the date when the binary was built
	Date = "n/a"
	// Version is the version of the binary
	Version = "n/a"
)
