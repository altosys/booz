package utils

import (
	"context"
	"reflect"
	"syscall"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestStartInterruptListener(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	// interrupt using SIGINT
	go StartInterruptListener(ctx, cancel)
	time.Sleep(200 * time.Millisecond)
	assert.NoError(t, syscall.Kill(syscall.Getpid(), syscall.SIGINT))
	<-ctx.Done()

	// interrupt using context cancel
	ctx, cancel = context.WithCancel(context.Background())
	go StartInterruptListener(ctx, cancel)
	time.Sleep(200 * time.Millisecond)
	cancel()
	<-ctx.Done()
}

func TestRemoveEmptySliceElements(t *testing.T) {
	type args struct {
		s []string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "no changes to non-empty strings",
			args: args{
				s: []string{"!@#", "abc", "123"},
			},
			want: []string{"!@#", "abc", "123"},
		},
		{
			name: "remove all empty strings",
			args: args{
				s: []string{"!@#", "", "123", ""},
			},
			want: []string{"!@#", "123"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RemoveEmptySliceElements(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RemoveEmptySliceElements() = %v, want %v", got, tt.want)
			}
		})
	}
}
