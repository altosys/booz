package utils

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
)

// StartInterruptListener listens for SIGINT to handle graceful shutdown
func StartInterruptListener(ctx context.Context, cancel context.CancelFunc) {
	signalCh := make(chan os.Signal, 1)
	defer close(signalCh)
	signal.Notify(signalCh, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	select {
	case <-signalCh:
		log.Println("interrupt signal received, shutting down...")
		signal.Stop(signalCh)
		cancel()
	case <-ctx.Done():
		log.Println("stopping interrupt signal listener")
		signal.Stop(signalCh)
	}
}

func RemoveEmptySliceElements(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}
