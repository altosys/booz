package notify

import (
	"booz"
	"fmt"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/gregdel/pushover"
	"github.com/stretchr/testify/assert"
)

func TestNewNotifier(t *testing.T) {
	type args struct {
		pushoverUserKey  string
		pushoverApiToken string
	}
	tests := []struct {
		name string
		args args
		want Notifier
	}{
		{
			name: "return new notifier",
			args: args{
				pushoverUserKey:  "key",
				pushoverApiToken: "token",
			},
			want: Notifier{
				app:       pushover.New("token"),
				recipient: pushover.NewRecipient("key"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewNotifier(tt.args.pushoverUserKey, tt.args.pushoverApiToken); !reflect.DeepEqual(
				got,
				tt.want,
			) {
				t.Errorf("NewNotifier() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNotifier_NotifyReleaseProducts(t *testing.T) {
	type fields struct {
		app       *pushover.Pushover
		recipient *pushover.Recipient
	}
	type args struct {
		products []booz.Product
		release  *booz.Release
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "return error on failed send",
			args: args{
				products: []booz.Product{},
				release: &booz.Release{
					Date: time.Now().UTC(),
				},
			},
			fields: fields{
				app:       pushover.New("invalid"),
				recipient: pushover.NewRecipient("invalid"),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &Notifier{
				app:       tt.fields.app,
				recipient: tt.fields.recipient,
			}
			if err := n.NotifyReleaseProducts(tt.args.products, tt.args.release); (err != nil) != tt.wantErr {
				t.Errorf("Notifier.NotifyReleaseProducts() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestNotifier_WriteMessage(t *testing.T) {
	type fields struct {
		app       *pushover.Pushover
		recipient *pushover.Recipient
	}
	type args struct {
		products []booz.Product
		release  *booz.Release
	}
	date := time.Date(2000, time.January, 1, 0, 0, 0, 0, time.UTC)
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *pushover.Message
	}{
		{
			name: "return message with no products found",
			args: args{
				products: []booz.Product{},
				release: &booz.Release{
					Type: booz.WebRelease,
					Date: date,
				},
			},
			want: &pushover.Message{
				Title:    "Systembolaget Web Release 1/1",
				Message:  "No malt whisky found",
				URL:      "",
				URLTitle: "View release on systembolaget.se",
				HTML:     true,
			},
		},
		{
			name: "return message with products found",
			args: args{
				products: []booz.Product{
					{
						Brand:    "brand",
						Country:  "country",
						Bottling: "bottling",
						ID:       00000,
						Price:    1,
						Volume:   10,
						ABV:      100,
					},
				},
				release: &booz.Release{
					Type: booz.WebRelease,
					Date: date,
					URL:  "url",
				},
			},
			want: &pushover.Message{
				Title: "Systembolaget Web Release 1/1",
				Message: strings.TrimSpace(fmt.Sprintf(
					"<a href=https://www.systembolaget.se/sok/?textQuery=%v>%s %s</a>\n %vml %.1f%% %vkr\n",
					00000,
					"brand",
					"bottling",
					10,
					float32(100),
					1,
				)),
				URL:      "url",
				URLTitle: "View release on systembolaget.se",
				HTML:     true,
			},
		},
		{
			name: "return truncated message with products exceeding max message length",
			args: args{
				products: []booz.Product{
					{
						Brand:    strings.Repeat("x", 800),
						Country:  "country",
						Bottling: "bottling",
						ID:       00000,
						Price:    1,
						Volume:   10,
						ABV:      100,
					},
					{
						Brand:    strings.Repeat("y", 800),
						Country:  "country",
						Bottling: "bottling",
						ID:       00000,
						Price:    1,
						Volume:   10,
						ABV:      100,
					},
				},
				release: &booz.Release{
					Type: booz.WebRelease,
					Date: date,
					URL:  "url",
				},
			},
			want: &pushover.Message{
				Title: "Systembolaget Web Release 1/1",
				Message: strings.TrimSpace(fmt.Sprintf(
					"<a href=https://www.systembolaget.se/sok/?textQuery=%v>%s %s</a>\n %vml %.1f%% %vkr\n",
					00000,
					strings.Repeat("x", 800),
					"bottling",
					10,
					float32(100),
					1,
				)) + "\n\n(+1 more)",
				URL:      "url",
				URLTitle: "View release on systembolaget.se",
				HTML:     true,
			},
		},
		{
			name: "return truncated message with trunc line exceeding max message length",
			args: args{
				products: []booz.Product{
					{
						Brand: strings.Repeat("x", 850),
						// Brand:    "x",
						Country:  "country",
						Bottling: "bottling",
						ID:       00000,
						Price:    1,
						Volume:   10,
						ABV:      100,
					},
					{
						Brand:    "y",
						Country:  "country",
						Bottling: "bottling",
						ID:       00000,
						Price:    1,
						Volume:   10,
						ABV:      100,
					},
					{
						Brand:    "z",
						Country:  "country",
						Bottling: "bottling",
						ID:       00000,
						Price:    1,
						Volume:   10,
						ABV:      100,
					},
				},
				release: &booz.Release{
					Type: booz.SmallBatchRelease,
					Date: date,
					URL:  "url",
				},
			},
			want: &pushover.Message{
				Title: "Systembolaget Small Batches 1/1",
				Message: strings.TrimSpace(fmt.Sprintf(
					"<a href=https://www.systembolaget.se/sok/?textQuery=%v>%s %s</a>\n %vml %.1f%% %vkr\n\n(+2 more)",
					00000,
					strings.Repeat("x", 850),
					"bottling",
					10,
					float32(100),
					1,
				)),
				URL:      "url",
				URLTitle: "View release on systembolaget.se",
				HTML:     true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &Notifier{
				app:       tt.fields.app,
				recipient: tt.fields.recipient,
			}

			got := n.writeMessage(tt.args.products, tt.args.release)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestNotifier_SendMessage(t *testing.T) {
	type fields struct {
		app       *pushover.Pushover
		recipient *pushover.Recipient
	}
	type args struct {
		message *pushover.Message
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "return error on invalid token",
			fields: fields{
				app:       pushover.New("invalid"),
				recipient: pushover.NewRecipient("invalid"),
			},
			args: args{
				message: &pushover.Message{Message: ""},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			n := &Notifier{
				app:       tt.fields.app,
				recipient: tt.fields.recipient,
			}
			if err := n.sendMessage(tt.args.message); (err != nil) != tt.wantErr {
				t.Errorf("Notifier.SendMessage() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
