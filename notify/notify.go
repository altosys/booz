package notify

import (
	"booz"
	"fmt"
	"log"
	"strings"

	"github.com/gregdel/pushover"
	"github.com/pkg/errors"
)

// Config holds configuration parameters for notifications
type Config struct {
	PushoverUserKey, PushoverAPIToken string
}

// Notifier is a notification sender
type Notifier struct {
	app       *pushover.Pushover
	recipient *pushover.Recipient
}

func NewNotifier(pushoverUserKey, pushoverApiToken string) Notifier {
	app := pushover.New(pushoverApiToken)
	recipient := pushover.NewRecipient(pushoverUserKey)
	n := Notifier{
		app:       app,
		recipient: recipient,
	}
	return n
}

func (n *Notifier) NotifyReleaseProducts(products []booz.Product, release *booz.Release) error {
	msg := n.writeMessage(products, release)
	if err := n.sendMessage(msg); err != nil {
		return errors.Wrap(err, "notify.NotifyReleaseProducts")
	}
	return nil
}

func (n *Notifier) writeMessage(
	products []booz.Product,
	release *booz.Release,
) *pushover.Message {
	sb := strings.Builder{}

	for i, p := range products {
		line := fmt.Sprintf(
			"<a href=https://www.systembolaget.se/sok/?textQuery=%v>%s %s</a>\n %vml %.1f%% %vkr\n",
			p.ID, p.Brand, p.Bottling, p.Volume, p.ABV, p.Price,
		)
		trunc := fmt.Sprintf("\n(+%v more)", len(products)-i)

		// write trunc line if writing new line exceeds max message length
		if pushover.MessageMaxLength < sb.Len()+len(line) {
			sb.WriteString(trunc)
			break
		}

		// write trunc line if trunc line won't fit after writing new line and there are more products left
		if (pushover.MessageMaxLength-(sb.Len()+len(line))) < len(trunc) && i != len(products)-1 {
			fmt.Fprint(&sb, trunc)
			break
		}
		sb.WriteString(line)
	}

	if len(products) == 0 {
		sb.WriteString("No malt whisky found")
	}

	var titleReleaseType string
	if release.Type == booz.WebRelease {
		titleReleaseType = "Web Release"
	}
	if release.Type == booz.SmallBatchRelease {
		titleReleaseType = "Small Batches"
	}

	message := pushover.Message{
		Title:    fmt.Sprintf("Systembolaget %s %s", titleReleaseType, release.Date.Format("2/1")),
		Message:  strings.TrimSpace(sb.String()),
		URL:      release.URL,
		URLTitle: "View release on systembolaget.se",
		HTML:     true,
	}
	return &message
}

func (n *Notifier) sendMessage(message *pushover.Message) error {
	if _, err := n.app.SendMessage(message, n.recipient); err != nil {
		return errors.Wrap(err, "notify.SendMessage")
	}
	log.Println("notification sent")
	return nil
}
