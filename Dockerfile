FROM docker.io/chromedp/headless-shell:113.0.5672.12
COPY booz /opt/booz
RUN chmod a+x /opt/booz
RUN apt-get update && apt-get install -y ca-certificates tzdata
ENTRYPOINT ["/opt/booz"]
CMD ["run"]
