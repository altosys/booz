package config

import (
	"os"
	"reflect"
	"testing"
	"time"
)

func TestNewConfig(t *testing.T) {
	type args struct {
		opts []ConfigOption
	}
	tz, _ := time.LoadLocation("Europe/Stockholm")
	tests := []struct {
		name string
		args args
		want *Config
	}{
		{
			name: "new config with default values",
			args: args{[]ConfigOption{}},
			want: &Config{
				schedule: "0 9 * * 3",
				timeZone: time.UTC,
			},
		},
		{
			name: "new config with custom values",
			args: args{[]ConfigOption{
				WithSchedule("* * * * *"),
				WithPushoverAPIToken("token"),
				WithPushoverUserKey("key"),
				WithTimeZone(tz),
				WithSmallBatchReleases(true),
				WithWebReleases(true),
			}},
			want: &Config{
				schedule:                "* * * * *",
				timeZone:                tz,
				pushoverAPIToken:        "token",
				pushoverUserKey:         "key",
				watchSmallBatchReleases: true,
				watchWebReleases:        true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewConfig(tt.args.opts...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewConfig() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestConfig_GetSchedule(t *testing.T) {
	type fields struct {
		schedule string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "get config schedule",
			fields: fields{
				schedule: "* * * * *",
			},
			want: "* * * * *",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Config{
				schedule: tt.fields.schedule,
			}
			if got := c.GetSchedule(); got != tt.want {
				t.Errorf("Config.GetSchedule() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestConfig_GetPushoverAPIToken(t *testing.T) {
	type fields struct {
		pushoverAPIToken string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "get config pushover api token",
			fields: fields{
				pushoverAPIToken: "token",
			},
			want: "token",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Config{
				pushoverAPIToken: tt.fields.pushoverAPIToken,
			}
			if got := c.GetPushoverAPIToken(); got != tt.want {
				t.Errorf("Config.GetPushoverAPIToken() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestConfig_GetPushoverUserKey(t *testing.T) {
	type fields struct {
		pushoverUserKey string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "get config pushover user key",
			fields: fields{
				pushoverUserKey: "key",
			},
			want: "key",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Config{
				pushoverUserKey: tt.fields.pushoverUserKey,
			}
			if got := c.GetPushoverUserKey(); got != tt.want {
				t.Errorf("Config.GetPushoverUserKey() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestConfig_GetSmallBatchReleasesEnabled(t *testing.T) {
	type fields struct {
		watchSmallBatchReleases bool
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "get config small batches enabled false",
			fields: fields{
				watchSmallBatchReleases: false,
			},
			want: false,
		},
		{
			name: "get config small batches enabled true",
			fields: fields{
				watchSmallBatchReleases: true,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Config{
				watchSmallBatchReleases: tt.fields.watchSmallBatchReleases,
			}
			if got := c.GetSmallBatchReleasesEnabled(); got != tt.want {
				t.Errorf("Config.GetSmallBatchReleasesEnabled() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestConfig_GetWebReleasesEnabled(t *testing.T) {
	type fields struct {
		watchWebReleases bool
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "get config small batches enabled false",
			fields: fields{
				watchWebReleases: false,
			},
			want: false,
		},
		{
			name: "get config small batches enabled true",
			fields: fields{
				watchWebReleases: true,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Config{
				watchWebReleases: tt.fields.watchWebReleases,
			}
			if got := c.GetWebReleasesEnabled(); got != tt.want {
				t.Errorf("Config.GetWebReleasesEnabled() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestConfig_GetTimeZone(t *testing.T) {
	type fields struct {
		timeZone *time.Location
	}
	tests := []struct {
		name   string
		fields fields
		want   *time.Location
	}{
		{
			name: "get config time zone",
			fields: fields{
				timeZone: time.UTC,
			},
			want: time.UTC,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Config{
				timeZone: tt.fields.timeZone,
			}
			if got := c.GetTimeZone(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Config.GetTimeZone() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLoad(t *testing.T) {
	tests := []struct {
		name    string
		env     map[string]string
		opts    *LoadOptions
		want    *Config
		wantErr bool
	}{
		{
			name:    "return error on no env vars set",
			opts:    &LoadOptions{},
			want:    &Config{},
			wantErr: true,
		},
		{
			name: "return error on invalid time zone",
			env: map[string]string{
				"BOOZ_TZ": "invalid",
			},
			opts:    &LoadOptions{},
			want:    &Config{},
			wantErr: true,
		},
		{
			name: "return error on invalid schedule",
			env: map[string]string{
				"BOOZ_TZ":       "UTC",
				"BOOZ_SCHEDULE": "invalid",
			},
			opts:    &LoadOptions{},
			want:    &Config{},
			wantErr: true,
		},
		{
			name: "return error on pushover vars not set",
			env: map[string]string{
				"BOOZ_TZ":       "UTC",
				"BOOZ_SCHEDULE": "* * * * *",
			},
			opts:    &LoadOptions{},
			want:    &Config{},
			wantErr: true,
		},
		{
			name: "return error when no release type enabled",
			env: map[string]string{
				"BOOZ_TZ":                 "UTC",
				"BOOZ_SCHEDULE":           "* * * * *",
				"BOOZ_PUSHOVER_API_TOKEN": "token",
				"BOOZ_PUSHOVER_USER_KEY":  "key",
			},
			opts:    &LoadOptions{},
			want:    &Config{},
			wantErr: true,
		},
		{
			name: "skip pushover credentials check",
			env: map[string]string{
				"BOOZ_TZ":                   "UTC",
				"BOOZ_SCHEDULE":             "* * * * *",
				"BOOZ_PUSHOVER_API_TOKEN":   "",
				"BOOZ_PUSHOVER_USER_KEY":    "",
				"BOOZ_WEB_RELEASES":         "true",
				"BOOZ_SMALL_BATCH_RELEASES": "true",
			},
			opts: &LoadOptions{SkipPushoverCredentials: true},
			want: &Config{
				timeZone:                time.UTC,
				schedule:                "* * * * *",
				pushoverAPIToken:        "",
				pushoverUserKey:         "",
				watchSmallBatchReleases: true,
				watchWebReleases:        true,
			},
			wantErr: false,
		},
		{
			name: "return config on success",
			env: map[string]string{
				"BOOZ_TZ":                   "UTC",
				"BOOZ_SCHEDULE":             "* * * * *",
				"BOOZ_PUSHOVER_API_TOKEN":   "token",
				"BOOZ_PUSHOVER_USER_KEY":    "key",
				"BOOZ_WEB_RELEASES":         "true",
				"BOOZ_SMALL_BATCH_RELEASES": "true",
			},
			opts: &LoadOptions{},
			want: &Config{
				timeZone:                time.UTC,
				schedule:                "* * * * *",
				pushoverAPIToken:        "token",
				pushoverUserKey:         "key",
				watchSmallBatchReleases: true,
				watchWebReleases:        true,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for k, v := range tt.env {
				os.Setenv(k, v)
			}
			got, err := Load(tt.opts)
			if (err != nil) != tt.wantErr {
				t.Errorf("Load() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Load() = %v, want %v", got, tt.want)
			}
		})
	}
}
