package config

import (
	"fmt"
	"time"

	"github.com/adhocore/gronx"
	"github.com/spf13/viper"
)

type LoadOptions struct {
	SkipPushoverCredentials bool
}

type Config struct {
	schedule                string
	pushoverAPIToken        string
	pushoverUserKey         string
	timeZone                *time.Location
	watchSmallBatchReleases bool
	watchWebReleases        bool
}

type ConfigOption func(*Config)

func NewConfig(opts ...ConfigOption) *Config {
	const (
		defaultSchedule = "0 9 * * 3"
	)
	defaultTimeZone := time.UTC

	config := &Config{
		timeZone: defaultTimeZone,
		schedule: defaultSchedule,
	}

	for _, opt := range opts {
		opt(config)
	}
	return config
}

func (c *Config) GetSchedule() string {
	return c.schedule
}

func (c *Config) GetPushoverAPIToken() string {
	return c.pushoverAPIToken
}

func (c *Config) GetPushoverUserKey() string {
	return c.pushoverUserKey
}

func (c *Config) GetTimeZone() *time.Location {
	return c.timeZone
}

func (c *Config) GetSmallBatchReleasesEnabled() bool {
	return c.watchSmallBatchReleases
}

func (c *Config) GetWebReleasesEnabled() bool {
	return c.watchWebReleases
}

func WithSchedule(cron string) ConfigOption {
	return func(c *Config) {
		c.schedule = cron
	}
}

func WithPushoverAPIToken(token string) ConfigOption {
	return func(c *Config) {
		c.pushoverAPIToken = token
	}
}

func WithPushoverUserKey(key string) ConfigOption {
	return func(c *Config) {
		c.pushoverUserKey = key
	}
}

func WithSmallBatchReleases(enable bool) ConfigOption {
	return func(c *Config) {
		c.watchSmallBatchReleases = enable
	}
}

func WithWebReleases(enable bool) ConfigOption {
	return func(c *Config) {
		c.watchWebReleases = enable
	}
}

func WithTimeZone(tz *time.Location) ConfigOption {
	return func(c *Config) {
		c.timeZone = tz
	}
}

func Load(loadOpts *LoadOptions) (*Config, error) {
	prefix := "BOOZ"
	viper.SetEnvPrefix(prefix)
	viper.AutomaticEnv()
	opts := []ConfigOption{}

	if viper.IsSet("TZ") {
		tz := viper.GetString("TZ")
		loc, err := time.LoadLocation(tz)
		if err != nil {
			return &Config{}, err
		}
		opts = append(opts, WithTimeZone(loc))
	}

	if viper.IsSet("SCHEDULE") {
		cron := viper.GetString("SCHEDULE")
		gron := gronx.New()
		if !gron.IsValid(cron) {
			return &Config{}, fmt.Errorf("invalid cron expression '%s'", cron)
		}
		opts = append(opts, WithSchedule(cron))
	}

	if !loadOpts.SkipPushoverCredentials {
		for _, s := range []string{"PUSHOVER_API_TOKEN", "PUSHOVER_USER_KEY"} {
			if !viper.IsSet(s) {
				return &Config{}, fmt.Errorf("required variable %s_%s is not set", prefix, s)
			}
		}
	}

	if !viper.IsSet("SMALL_BATCH_RELEASES") && !viper.IsSet("WEB_RELEASES") {
		return &Config{}, fmt.Errorf(
			"BOOZ_WEB_RELEASES or BOOZ_SMALL_BATCH_RELEASES must be set to 'true'",
		)
	}

	pushoverAPIToken := viper.GetString("PUSHOVER_API_TOKEN")
	pushoverUserKey := viper.GetString("PUSHOVER_USER_KEY")
	enableWebReleases := viper.GetBool("WEB_RELEASES")
	enableSmallBatchReleases := viper.GetBool("SMALL_BATCH_RELEASES")
	opts = append(opts, WithPushoverAPIToken(pushoverAPIToken))
	opts = append(opts, WithPushoverUserKey(pushoverUserKey))
	opts = append(opts, WithWebReleases(enableWebReleases))
	opts = append(opts, WithSmallBatchReleases(enableSmallBatchReleases))
	return NewConfig(opts...), nil
}
