package mock

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

type Endpoint struct {
	Path     string
	Response []byte
}

func HTTPServer(t *testing.T, endpoints []Endpoint) *httptest.Server {
	mux := http.NewServeMux()

	for _, e := range endpoints {
		e := e
		mux.HandleFunc(e.Path, func(res http.ResponseWriter, req *http.Request) {
			_, err := res.Write(e.Response)
			assert.NoError(t, err)
		})
	}

	return httptest.NewServer(mux)
}
