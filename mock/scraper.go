package mock

import (
	"booz"
	"time"
)

type Scraper struct {
	getReleaseURLFn func(date time.Time) string
	parseResultFn   func(release []string) []booz.Product
	ScrapeReleaseFn func(release *booz.Release) ([]booz.Product, error)
}

// GetScraper returns a mock scraper
func GetScraper() Scraper {
	scrapeReleaseFn := func(release *booz.Release) ([]booz.Product, error) {
		return []booz.Product{}, nil
	}

	releaseURLFn := func(date time.Time) string {
		return ""
	}

	parseResultFn := func(result []string) []booz.Product {
		return []booz.Product{}
	}

	return Scraper{
		getReleaseURLFn: releaseURLFn,
		parseResultFn:   parseResultFn,
		ScrapeReleaseFn: scrapeReleaseFn,
	}
}
