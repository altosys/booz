package scraper

import (
	"booz"
	"booz/utils"
	"context"
	"fmt"
	"log"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/chromedp/chromedp"
)

type (
	scraper struct{}
)

var (
	boozScraper booz.Scraper = &scraper{}
)

func GetScraper() booz.Scraper {
	return boozScraper
}

func buildReleaseURL(release *booz.Release) string {
	var releaseTypeURLParam string
	if release.Type == booz.SmallBatchRelease {
		releaseTypeURLParam = "Tillfälligt sortiment"
	}
	if release.Type == booz.WebRelease {
		releaseTypeURLParam = "Webblanseringar"
	}

	values := url.Values{}
	values.Add("saljstart-fran", release.Date.Format("2006-01-02"))
	values.Add("saljstart-till", release.Date.Format("2006-01-02"))
	values.Add("sortiment", releaseTypeURLParam)

	baseURL, _ := url.Parse("https://www.systembolaget.se")
	baseURL.Path += "/sortiment/sprit/whisky/maltwhisky/"
	baseURL.RawQuery = values.Encode()
	return baseURL.String()
}

// GetNextWeekdayDate gets the next future occurrence of the requested weekday
func getNextWeekdayDate(next time.Weekday) time.Time {
	now := time.Now()
	today := now.Weekday()
	var date time.Time

	if today <= next {
		date = now.AddDate(0, 0, int(next-today))
	}

	if today > next {
		date = now.AddDate(0, 0, int(next-today)+7)
	}
	return date
}

// UpdateReleases updates the release dates and URLs with the current upcoming ones
func UpdateReleases(releases []*booz.Release) {
	for _, r := range releases {
		if r.Type == booz.WebRelease {
			r.Date = getNextWeekdayDate(time.Thursday)
		}
		if r.Type == booz.SmallBatchRelease {
			r.Date = getNextWeekdayDate(time.Friday)
		}
		r.URL = buildReleaseURL(r)
	}
}

func (s *scraper) ScrapeRelease(release *booz.Release) ([]booz.Product, error) {
	ageConfirmElement := "a.e180zcpo2.css-dt7i4e.ev9wvac0"
	ageConfirmClickElement := "div.css-c6bmr8:nth-child(2) > a:nth-child(1)"
	cookieSelectionElement := ".css-b1zu0a"
	productElement := ".css-1otywyl"
	var result []string

	tasks := chromedp.Tasks{
		chromedp.EmulateViewport(2560, 1440),
		chromedp.ActionFunc(func(context.Context) error {
			log.Printf("loading systembolaget.se %s release", strings.ToLower(string(release.Type)))
			return nil
		}),
		chromedp.Navigate(release.URL),
		chromedp.ActionFunc(func(context.Context) error {
			log.Printf("loading page...")
			return nil
		}),
		chromedp.WaitVisible(ageConfirmElement, chromedp.ByQuery),
		chromedp.ActionFunc(func(context.Context) error {
			log.Printf("age confirmation found")
			return nil
		}),
		chromedp.Click(ageConfirmClickElement, chromedp.ByQuery),
		chromedp.ActionFunc(func(context.Context) error {
			log.Printf("age confirmation passed")
			return nil
		}),
		chromedp.WaitVisible(cookieSelectionElement, chromedp.ByQuery),
		chromedp.ActionFunc(func(context.Context) error {
			log.Printf("cookie selection found")
			return nil
		}),
		chromedp.Click(cookieSelectionElement, chromedp.ByQuery),
		chromedp.ActionFunc(func(context.Context) error {
			log.Printf("cookie selection passed")
			return nil
		}),
		chromedp.WaitVisible(productElement, chromedp.ByQuery),
		chromedp.ActionFunc(func(context.Context) error {
			log.Printf("found product element(s)")
			return nil
		}),
		chromedp.Evaluate(
			fmt.Sprintf(
				"[...document.querySelectorAll('%s')].map((e) => e.innerText)",
				productElement,
			),
			&result,
		),
	}

	ctx, _ := chromedp.NewContext(context.Background())
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	err := chromedp.Run(ctx, tasks)
	if err != nil && err.Error() != "context deadline exceeded" {
		return []booz.Product{}, err
	}
	return s.parseResult(result)
}

func (s *scraper) parseResult(result []string) ([]booz.Product, error) {
	products := []booz.Product{}
	for _, r := range result {
		fields := strings.Split(r, "\n")
		fields = utils.RemoveEmptySliceElements(fields)
		if len(fields) == 0 {
			continue
		}

		productType := strings.TrimSpace(strings.ToLower(fields[0]))
		r := regexp.MustCompile("maltwhiske?y")

		if r.MatchString(productType) {
			id, err := strconv.Atoi(strings.TrimPrefix(fields[3], "Nr "))
			if err != nil {
				return []booz.Product{}, err
			}

			volumeStrSlice := strings.Split(fields[5], " ")
			volume, err := strconv.Atoi(volumeStrSlice[len(volumeStrSlice)-2])
			if err != nil {
				return []booz.Product{}, err
			}

			abvStr := strings.Split(fields[6], "%")[0]
			abvStr = strings.ReplaceAll(abvStr, ",", ".")
			abvStr = strings.TrimSpace(abvStr)
			abv, err := strconv.ParseFloat(abvStr, 32)
			if err != nil {
				return []booz.Product{}, err
			}

			rgx := regexp.MustCompile("[^0-9]+")
			price, err := strconv.Atoi(rgx.ReplaceAllString(fields[7], ""))
			if err != nil {
				return []booz.Product{}, err
			}

			products = append(products, booz.Product{
				Brand:    fields[1],
				Bottling: fields[2],
				ID:       id,
				Country:  fields[4],
				Volume:   volume,
				ABV:      float32(abv),
				Price:    price,
			})
		}
	}
	return products, nil
}
