package scraper

import (
	"booz"
	"booz/mock"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestBuildReleaseURL(t *testing.T) {
	type args struct {
		release *booz.Release
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "get release url string",
			args: args{
				release: &booz.Release{
					Date: time.Now(),
					Type: booz.SmallBatchRelease,
				},
			},
			want: fmt.Sprintf(
				"https://www.systembolaget.se/sortiment/sprit/whisky/maltwhisky/?saljstart-fran=%s&saljstart-till=%s&sortiment=Tillf%%C3%%A4lligt+sortiment",
				time.Now().Format("2006-01-02"),
				time.Now().Format("2006-01-02"),
			),
		},
		{
			name: "get release url string",
			args: args{
				release: &booz.Release{
					Date: time.Now(),
					Type: booz.WebRelease,
				},
			},
			want: fmt.Sprintf(
				"https://www.systembolaget.se/sortiment/sprit/whisky/maltwhisky/?saljstart-fran=%s&saljstart-till=%s&sortiment=Webblanseringar",
				time.Now().Format("2006-01-02"),
				time.Now().Format("2006-01-02"),
			),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := buildReleaseURL(tt.args.release)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestGetNextWeekdayDate(t *testing.T) {
	type args struct {
		next time.Weekday
	}
	tests := []struct {
		name string
		args args
		want time.Time
	}{
		{
			name: "get next weekday occurrence on same week",
			args: args{
				next: time.Now().Weekday(),
			},
			want: time.Now(),
		},
		{
			name: "get next weekday occurrence on next week",
			args: args{
				next: time.Now().AddDate(0, 0, -1).Weekday(),
			},
			want: time.Now().AddDate(0, 0, 6),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := getNextWeekdayDate(tt.args.next)
			if got.Format("2006-01-02") != tt.want.Format("2006-01-02") {
				t.Errorf(
					"GetNextDayOfWeekDate() = %s, want %s",
					got.Format("2006-01-02"),
					tt.want.Format("2006-01-02"),
				)
			}
		})
	}
}

func TestScrapeRelease(t *testing.T) {
	tests := []struct {
		name      string
		endpoints map[string]string
		scraper   mock.Scraper
		want      []booz.Product
		wantErr   bool
	}{
		{
			name: "scrape products from release page",
			endpoints: map[string]string{
				"/":        "../internal/testing/scraper/testdata/age_confirmation.html",
				"/cookies": "../internal/testing/scraper/testdata/cookie_selection.html",
				"/release": "../internal/testing/scraper/testdata/release.html",
			},
			scraper: mock.GetScraper(),
			want: []booz.Product{
				{
					Brand:    "Loch Lomond",
					Country:  "Storbritannien",
					Bottling: "Inchmurrin",
					ID:       508,
					Price:    299,
					Volume:   700,
					ABV:      40,
				},
			},
			wantErr: false,
		},
		{
			name:      "return empty object when no matches found",
			endpoints: map[string]string{"/": "../internal/testing/scraper/testdata/empty.html"},
			scraper:   mock.GetScraper(),
			want:      []booz.Product{},
			wantErr:   false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			endpoints := []mock.Endpoint{}
			for path, file := range tt.endpoints {
				f, err := os.ReadFile(file)
				assert.NoError(t, err)
				endpoints = append(endpoints, mock.Endpoint{Path: path, Response: f})
			}
			ts := mock.HTTPServer(t, endpoints)
			defer ts.Close()

			tt.scraper.ScrapeReleaseFn = GetScraper().ScrapeRelease
			got, err := tt.scraper.ScrapeReleaseFn(&booz.Release{
				Type: booz.WebRelease,
				URL:  ts.URL,
			})
			assert.Equal(t, tt.wantErr, (err != nil))
			assert.Equal(t, tt.want, got)
		})
	}
}
